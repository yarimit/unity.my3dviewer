﻿using HoloToolkit.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectMenuCanvas : Singleton<ObjectMenuCanvas> {
	private ObjectController TargetObject_;

	// Use this for initialization
	void Start () {
		//GetComponent<Canvas>().enabled = false;
		SetEnabled(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void ResetSliders() {
		GetSlider("ScaleSlider").value = 1.0f;
		GetSlider("RotateXSlider").value = 0.0f;
		GetSlider("RotateYSlider").value = 0.0f;
		GetSlider("RotateZSlider").value = 0.0f;
	}

	void SetEnabled(bool enabled) {
		GetComponent<Canvas>().enabled = enabled;
	}

    public bool GetEnabled() {
        return GetComponent<Canvas>().enabled;
    }

	public void Show(ObjectController target) {
		TargetObject_ = target;
		SetEnabled(true);

		ResetSliders();
	}

	public void Hide() {
		SetEnabled(false);
	}

	private Slider GetSlider(string name) {
		Transform t = transform.Find("ObjectMenuWindow/" + name);
		return t.GetComponent<Slider>();
	}

	public void OnScaleSliderChanged() {
		float v = GetSlider("ScaleSlider").value;
		//Debug.LogFormat("OnScaleSliderChanged {0:F6}", v);
		if(TargetObject_ != null) {
			TargetObject_.UpdateScale(v);
		}
	}

	public void OnRotateXSliderChanged() {
		//float v = GetSlider("RotateXSlider").value;
		//Debug.LogFormat("OnRotateXSliderChanged {0:F6}", v);
		UpdateRotate();
	}

	public void OnRotateYSliderChanged() {
		UpdateRotate();
	}

	public void OnRotateZSliderChanged() {
		UpdateRotate();
	}

	public void UpdateRotate() {
		float x = GetSlider("RotateXSlider").value;
		float y = GetSlider("RotateYSlider").value;
		float z = GetSlider("RotateZSlider").value;

		if(TargetObject_ != null) {
			TargetObject_.UpdateRotation(x, y, z);
		}
	}

	public void OnMoveButton() {
		if(TargetObject_ != null) {
			TargetObject_.UpdateDraggable(true);
			Debug.Log("Set HandDraggable");
		}
	}
}
