﻿using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AppManager : Singleton<AppManager>, IInputClickHandler {
	public enum AppState {
		Default,        // デフォルト
	};

	private AppState AppState_ = AppState.Default;

	// Use this for initialization
	void Start () {
		InputManager.Instance.PushFallbackInputHandler(gameObject);
	}

    public AppState GetAppState() {
        return AppState_;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnInputClicked(InputClickedEventData eventData) {
		Debug.Log("Fallback InputClicked!");

		if(AppState_ == AppState.Default) {
			if (ObjectMenuCanvas.Instance.GetEnabled()) {
				ObjectMenuCanvas.Instance.Hide();
				return;
			}

            UnSelectPlacementObjects();

			InstantiateMenuCanvas.Instance.Toggle();
			return;
		}
	}

    public void InstantiateNewModel(GameObject prefab) {
        //AppState_ = AppState.Instantiating;

        // 初期位置
        GameObject go = Instantiate(prefab);
        go.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 2;
    }

    public void UnSelectPlacementObjects() {
        var objs = GameObject.FindGameObjectsWithTag("PlacementObject");
        foreach(var o in objs) {
            var oc2 = o.GetComponent<ObjectController2>();
            oc2.Select(false);
        }
    }
}
