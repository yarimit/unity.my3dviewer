﻿using HoloToolkit.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateMenuCanvas : Singleton<InstantiateMenuCanvas> {

	public GameObject CubePrefab_;
	public GameObject CylinderPrefab_;
	public GameObject ChairPrefab_;

	// Use this for initialization
	void Start () {
		SetEnabled(false);
	}

	// Update is called once per frame
	void Update () {
		
	}

	void SetEnabled(bool enabled) {
		GetComponent<Canvas>().enabled = enabled;
	}

	public bool GetEnabled() {
		return GetComponent<Canvas>().enabled;
	}

	public void Toggle() {
		SetEnabled(!GetEnabled());
	}

	public void OnInstantiateCube() {
		SetEnabled(false);
		AppManager.Instance.InstantiateNewModel(CubePrefab_);
	}

	public void OnInstantiateCylinder() {
		SetEnabled(false);
		AppManager.Instance.InstantiateNewModel(CylinderPrefab_);
	}

	public void OnInstantiateChair() {
		SetEnabled(false);
		AppManager.Instance.InstantiateNewModel(ChairPrefab_);
	}
}
