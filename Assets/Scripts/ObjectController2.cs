﻿using HoloToolkit.Unity.InputModule;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectController2 : MonoBehaviour, IInputClickHandler {
    private bool Selected_ = false;
    private List<MeshRenderer> DisabledRenderers_ = new List<MeshRenderer>();

    // Use this for initialization
    void Start () {
        disableHandles();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private MeshRenderer[] HandleRenderers() {
        var m1 = transform.Find("ScaleHandles").GetComponentsInChildren<MeshRenderer>();
        var m2 = transform.Find("RotateYHandles").GetComponentsInChildren<MeshRenderer>();

        int m1Len = m1.Length;
        Array.Resize(ref m1, m1.Length + m2.Length);
        Array.Copy(m2, 0, m1, m1Len, m2.Length);
        return m1;
    }

    private void disableHandles() {
        if(DisabledRenderers_.Count > 0) {
            return;
        }

        foreach (MeshRenderer renderer in HandleRenderers()) {
            if (renderer.enabled) {
                renderer.enabled = false;
                DisabledRenderers_.Add(renderer);
            }
        }

        //Debug.Log("Handles : " + HandleRenderers().Length);
    }

    private void enableHandles() {
        foreach (MeshRenderer renderer in DisabledRenderers_) {
            renderer.enabled = true;
        }
        DisabledRenderers_.Clear();
    }

    public void OnInputClicked(InputClickedEventData eventData) {
        //AppManager.Instance.UnSelectPlacementObjects();
        Select(true);
    }

    public void Select(bool selected) {
        Debug.LogFormat("Select : {0}", selected);
        Selected_ = selected;

        // ハンドルの表示ON/OFF
        if(Selected_) {
            enableHandles();
        } else {
            disableHandles();
        }

        // ドラッグ移動ON/OFF
        //GetComponent<HandDraggable>().IsDraggingEnabled = enabled;

    }
}
