﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ObjectController : MonoBehaviour, IInputHandler, IInputClickHandler {
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnInputDown(InputEventData eventData) {
		//Debug.Log("OnInputDown");
	}

	public void OnInputUp(InputEventData eventData) {
	}

	public void OnInputClicked(InputClickedEventData eventData) {
		Debug.Log("OnInputClicked");
		if (AppManager.Instance.GetAppState() != AppManager.AppState.Default) {
			return;
		}

		ManipulateBaseScale = transform.localScale;
		ManipulateBaseRotation = transform.localEulerAngles;
		this.UpdateDraggable(false);

		ObjectMenuCanvas.Instance.Show(this);
	}

	private Vector3 ManipulateBaseRotation;
	private Vector3 ManipulateBaseScale;

	public void UpdateScale(float v) {
		this.transform.localScale = ManipulateBaseScale * v;
	}

	public void UpdateRotation(float x, float y, float z) {
		Vector3 newRotate = ManipulateBaseRotation + new Vector3(x, y, z);
		//Debug.LogFormat("UpdateRotation : {0}", newRotate);
		//Debug.LogFormat("localEulerAngles : {0}", transform.localEulerAngles);
		//this.transform.Rotate(newRotate - transform.localEulerAngles);
		transform.localRotation = Quaternion.Euler(newRotate.x, newRotate.y, newRotate.z);
	}

	public void UpdateDraggable(bool enabled) {
		var d = GetComponent<HandDraggable>();
		d.IsDraggingEnabled = enabled;
	}
}