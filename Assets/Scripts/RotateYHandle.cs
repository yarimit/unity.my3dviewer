﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RotateYHandle : MonoBehaviour, IManipulationHandler {
    private float Speed = 2.0f;
    private GameObject BaseObject_;
    private Vector3 ManipulationPrevPos_;


    public void OnManipulationCanceled(ManipulationEventData eventData) {
        Debug.LogFormat("OnManipulationCanceled\r\nSource: {0}  SourceId: {1}\r\nCumulativeDelta: {2} {3} {4}",
            eventData.InputSource,
            eventData.SourceId,
            eventData.CumulativeDelta.x,
            eventData.CumulativeDelta.y,
            eventData.CumulativeDelta.z);
    }

    public void OnManipulationCompleted(ManipulationEventData eventData) {
        Debug.LogFormat("OnManipulationCompleted\r\nSource: {0}  SourceId: {1}\r\nCumulativeDelta: {2} {3} {4}",
            eventData.InputSource,
            eventData.SourceId,
            eventData.CumulativeDelta.x,
            eventData.CumulativeDelta.y,
            eventData.CumulativeDelta.z);
    }

    public void OnManipulationStarted(ManipulationEventData eventData) {
        //Debug.LogFormat("OnManipulationStarted\r\nSource: {0}  SourceId: {1}\r\nCumulativeDelta: {2} {3} {4}",
        //    eventData.InputSource,
        //    eventData.SourceId,
        //    eventData.CumulativeDelta.x,
        //    eventData.CumulativeDelta.y,
        //    eventData.CumulativeDelta.z);

        ManipulationPrevPos_ = eventData.CumulativeDelta;
    }

    public void OnManipulationUpdated(ManipulationEventData eventData) {
        //Debug.LogFormat("OnManipulationUpdated\r\nSource: {0}  SourceId: {1}\r\nCumulativeDelta: {2} {3} {4}",
        //    eventData.InputSource,
        //    eventData.SourceId,
        //    eventData.CumulativeDelta.x,
        //    eventData.CumulativeDelta.y,
        //    eventData.CumulativeDelta.z);

        float multiplier = 1.0f;
        float cameraLocalYRotation = Camera.main.transform.localRotation.eulerAngles.y;

        if (cameraLocalYRotation > 270 || cameraLocalYRotation < 90)
            multiplier = -1.0f;

        var rotation = new Vector3(0.0f, eventData.CumulativeDelta.x * multiplier);
        BaseObject_.transform.Rotate(rotation * Speed, Space.World);
        Debug.LogFormat("Maniputation Rotate : delta.x={0}, rotation.y={1}",
            eventData.CumulativeDelta.x, rotation.y);
    }

    // Use this for initialization
    void Start () {
        BaseObject_ = transform.parent.parent.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
